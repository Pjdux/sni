import React, { useState, useEffect } from "react";
import FormWrap from "../../styled-components/FormWrap";
import MainLayout from "../../styled-components/Main";
import { AgeOptions } from "./options/AgeOptions";

import { Form, FormField, FormInput } from "semantic-ui-react";
import Select from "react-select";

const ClientForm = () => {
  const [formData, setFormData] = useState({});
  const [options, setOptions] = useState({});
  const [message, setMessage] = useState("");

  const handleSelect = (name, selectedOptions) => {
    setOptions({ ...formData, [name]: selectedOptions.value });
    //console.log("This Are the selected options", selectedOptions);
  };

  const handleSelections = (name, selectedcarOptions) => {
    //console.log("selectedcarOptions", selectedcarOptions);

    setFormData({ ...formData, [name]: selectedcarOptions.label });

    //console.log("this is the maker", formData.maker);
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    if (formData.Age < 25) {
      setMessage("You are not Old Enough");
    } else {
      console.log("this is the form data ", formData);
    }
  };

  const handleChange = (e) => {
    e.preventDefault();

    setFormData({
      ...formData,
      [e.target.name]: e.target.value,
    });
  };

  return (
    <MainLayout>
      {message}
      <FormWrap onSubmit={handleSubmit}>
        <FormField>
          <FormInput
            type="text"
            name="firstName"
            placeholder="Last Name"
            onChange={handleChange}
          />
        </FormField>
        <FormField></FormField>
        <FormField>
          <FormInput
            typ="text"
            name="lastName"
            placeholder="Last Name"
            onChange={handleChange}
          />
          <Select
            className="select"
            name="Age"
            options={AgeOptions}
            onChange={(selected) => handleSelections("Age", selected)}
          />
          <FormInput type="submit" />
        </FormField>
      </FormWrap>
    </MainLayout>
  );
};

export default ClientForm;
