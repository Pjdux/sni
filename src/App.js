import React from "react";
import MainLayout from "./styled-components/Main";
import "./App.css";
import Story from "./containers/Story";
import ClientForm from "./containers/forms/ClientForm";

function App() {
  return (
    <MainLayout className="App">
      <Story />
    </MainLayout>
  );
}

export default App;
